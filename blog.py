import os
import webapp2
import jinja2
import datetime
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=False)

#utilities
html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&apos;",
    ">": "&gt;",
    "<": "&lt;",
    }


def escape_html(s):
    """Produce entities within text."""
    line = ""
    for c in s:
        line = line+ html_escape_table.get(c,c)

    return line.replace("\n", "<br>")


class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

class Post(db.Model):
    subject = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    modified = db.DateTimeProperty(auto_now = True)

class MainPage(Handler):
    def render_front(self):
	posts = db.GqlQuery("SELECT * FROM Post ORDER BY created DESC")
	self.render("front.html", posts= posts)

    def get(self):
        self.render_front()

class NewPost(Handler):
	def render_newpost(self, subject="", content="", error=""):
		self.render("newpost.html", subject=subject, content=content, error=error)

	def get(self):
		self.render_newpost()

	def post(self):
		subject = self.request.get("subject")
		content = self.request.get("content")

		if subject and content:
			content = escape_html(content)
			p = Post(subject=subject, content=content)
			#p.created = datetime.datetime.now().date()
			p.put()

			self.redirect("/blog/"+ str(p.key().id()))

		else:
			error = "we need both a subject and content!"
			self.render_newpost(subject, content, error)

class PostPermalink(Handler):
	def get(self, post_id):
		entry = Post.get_by_id(int(post_id))

		if entry:
			self.render("post.html", subject=entry.subject, content=entry.content, date=entry.created)


app = webapp2.WSGIApplication([('/blog', MainPage), ('/blog/newpost', NewPost), (r'/blog/(\d+)', PostPermalink)], debug=True)


